<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <title>PillTime - Manual Project</title>

    <!-- Meta -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Search Engine Noindex -->
    <meta name="robots" content="noindex, nofollow">
    <meta name="googlebot" content="noindex">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#0093D7">
    <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#0093D7">

    <!-- Browser Reset -->
    <link rel="stylesheet" type="text/css" href="public/scss/reset.min.css">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

    <!-- Variables -->
    <link rel="stylesheet" type="text/css" href="public/scss/vars.min.css">

    <!-- Variables -->
    <link rel="stylesheet" type="text/css" href="public/scss/main.min.css">

    <!-- Scripts -->
    <script type="text/javascript" src="models/orders.js"></script>
    <script type="text/javascript" src="models/stock.js"></script>
    <script defer type="text/javascript" src="node_modules/underscore/underscore-min.js"></script>
    <script defer type="text/javascript" src="controllers/orderController.js"></script>
    <script defer type="text/javascript" src="controllers/loginController.js"></script>
    <script defer type="text/javascript" src="controllers/stockController.js"></script>
    <script defer type="text/javascript" src="controllers/reportsController.js"></script>

    <script defer type="text/javascript" src="controllers/dashboardController.js"></script>
    <script async defer type="text/javascript" src="vendor/chart.min.js"></script>
    <script async defer type="text/javascript" src="vendor/chart.bundle.min.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZ2lb31ANdQbx7kA6n8jGvxuJlA6EGOxg&callback=setMaps"></script>


  </head>
  <body>

    <?php include 'includes/login.php' ?>

    <?php include 'includes/dashboard.php' ?>

    <?php include 'includes/orders-1.php' ?>

    <?php include 'includes/stock-1.php' ?>

    <?php include 'includes/admin-1.php' ?>

    <?php include 'includes/reports-1.php' ?>

  </body>
</html>
