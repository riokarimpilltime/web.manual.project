(function(window){
  "use strict";
  function Orders()
  {
    this.data = [
      {
        orderId: 1,
        givenOrderId: 1,
        givenId: 1,
        fullName: 'Reza Moghbel',
        addressLine1: '3 Maxwell Road',
        addressLine2: 'Rumney',
        area: 'South Glamorgan',
        city: 'Cardiff',
        country: 'GB',
        postcode: 'CF33NA',
        telNumber: '07222111333',
        email: 'rez.mog@pilltime.co.uk',
        preferredName: 'Rezzy',
      },
      {
        orderId: 2,
        givenOrderId: 3,
        givenId: 4,
        fullName: 'Rio Karim',
        addressLine1: '20 Hafod Street',
        addressLine2: '',
        area: 'South Glamorgan',
        city: 'Cardiff',
        country: 'GB',
        postcode: 'CF81 8PB',
        telNumber: '077111222333',
        email: 'rio.karim@pilltime.co.uk',
        preferredName: '',
      }
    ]
  }
  if(!window.stock)
    window.Orders = new Orders();
})(window);
