(function(window){
  "use strict";
  function Stock()
  {
    this.data = [
      {
        id: 2,
        name: 'Paracetomal',
        stockQuantity: 1,
        manualBarcode: 1211231234,
        pilltimeBarcode: 222,
        packQuantity: 56,
        weight: 250 + 'g',
        durationPeriod: 56,
      },
      {
        id: 2,
        name: 'Codeine',
        stockQuantity: 1234,
        manualBarcode: 12123898534,
        pilltimeBarcode: 'YYY',
        packQuantity: 56,
        weight: 250,
        durationPeriod: 56,
      },
      {
        id: 2,
        name: 'Phenylepharine',
        stockQuantity: 121,
        manualBarcode: 1212374899,
        pilltimeBarcode: 'ZZZ',
        packQuantity: 56,
        weight: 250,
        durationPeriod: 56,
      },
      {
        id: 2,
        name: 'Aspirin',
        stockQuantity: 9991,
        manualBarcode: 1321224891,
        pilltimeBarcode: 'A2A',
        packQuantity: 56,
        weight: 250,
        durationPeriod: 56,
      },
      {
        id: 1,
        name: 'Ibuprofen',
        stockQuantity: 13,
        manualBarcode: 1212144128,
        pilltimeBarcode: 'GG2',
        packQuantity: 589,
        weight: 310,
        durationPeriod: 90,
      }
    ]
  }
  if(!window.stock)
    window.Stock = new Stock();
})(window);
