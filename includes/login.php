<main class="login flex-center">
  <div class="operating-system-window">
    <span class="close-btn dot"></span>
    <span class="dot"></span>
    <span class="dot"></span>
  </div>
  <div class="container">
    <div class="header">
      <span>Manual Login</span>
    </div>
    <div class="form-group">
      <label for="username">Email</label>
      <input type="email" id="inputEmail" name="email" value="">
    </div>
    <div class="form-group">
      <label for="password">Password</label>
      <input type="password" name="password" value="">

    </div>
    <div class="btn-group btn-group-column">
      <button data-handler="loginSubmit" class="btn btn-primary">Login</button>
      <!--<span class="btn btn-secondary">Clear</span>
      <span class="btn btn-link"><span>Forgot Password?</span></span>-->
    </div>
  </div>
</main>
