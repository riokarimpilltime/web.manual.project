<main class="reports-1 flex-center">
  <div class="operating-system-window">
    <span class="close-btn dot"></span>
    <span class="dot"></span>
    <span class="dot"></span>
  </div>
  <div class="container">
    <?php include 'nav.php' ?>
    <div class="content">
        <div class="content-wrapper content-wrapper-graph">
          <?php include 'filter-graph.php' ?>
          <div class="tooltip tooltip-help aa">
            <span>Select a stock from the list and edit it it's stock levels.</span>
            <img src="public/res/information.svg" alt="">
          </div>
          <div class="header">
            Orders (Weekly)
          </div>
          <canvas id="ordersChart" class="order-chart"></canvas>
        </div>

        <div id="ordersMap" class="content-wrapper content-wrapper-map">

        </div>
      </div>
  </div>
</main>
