<main class="orders-1 flex-center">
  <div class="operating-system-window">
    <span class="close-btn dot"></span>
    <span class="dot"></span>
    <span class="dot"></span>
  </div>
  <div class="container">
    <?php include 'nav.php' ?>
    <div class="content">
      <div class="content-wrapper content-wrapper-scan">
        <div class="tooltip tooltip-help">
          <span>Select a stock from the list and edit it it's stock levels.</span>
          <img src="public/res/information.svg" alt="">
        </div>
        <div class="info-user">
          <div class="user-content">
            <div class="user-header">
              <img src="public/res/avatar.svg" alt="">
            </div>
            <div class="user-header">
              <span class="user-name"></span>
            </div>
          </div>
          <div class="user-content">
            <div class="form-group">
              <div class="label">

              </div>
              <div class="data">

              </div>
            </div>
            <div class="form-group">
              <div class="label">

              </div>
              <div class="data">

              </div>
            </div>
            <div class="form-group">
              <div class="label">

              </div>
              <div class="data">

              </div>
            </div>
            <div class="form-group">
              <div class="label">

              </div>
              <div class="data">

              </div>
            </div>
            <div class="form-group">
              <div class="label">

              </div>
              <div class="data">

              </div>
            </div>
            <div class="form-group">
              <div class="label">

              </div>
              <div class="data">

              </div>
            </div>
            <div class="form-group">
              <div class="label">

              </div>
              <div class="data">

              </div>
            </div>
            <div class="form-group">
              <div class="label">

              </div>
              <div class="data">

              </div>
            </div>
            <div class="form-group">
              <div class="label">

              </div>
              <div class="data">

              </div>
            </div>
            <div class="form-group">
              <div class="label">

              </div>
              <div class="data">

              </div>
            </div>
            <div class="form-group">
              <div class="label">

              </div>
              <div class="data">

              </div>
            </div>
            <div class="form-group">
              <div class="label">

              </div>
              <div class="data">

              </div>
            </div>
            <div class="form-group">
              <div class="label">

              </div>
              <div class="data">

              </div>
            </div>
          </div>
        </div>
        <div class="info-rx">
          <div class="header">
            Locate Items...
          </div>
          <div class="rx-content">
            <div class="rx-package">
                <div class="rx-package-icon">
                  <img src="public/res/ibuprofen.png" alt="">
                </div>
                <div class="rx-package-name"><span>Ibuprofen</span></div>
            </div>
            <div class="rx-package">
                <div class="rx-package-icon">
                  <img src="public/res/paracetamol.png" alt="">
                </div>
                <div class="rx-package-name"><span>Paracetamol</span></div>
            </div>
            <div class="rx-package">
                <div class="rx-package-icon">
                  <div class="rx-package-icon">
                    <img src="public/res/phenylephrine.png" alt="">
                  </div>
                </div>
                <div class="rx-package-name"><span>Phenylephrine</span></div>
            </div>
            <div class="rx-package">
                <div class="rx-package-icon">
                  <div class="rx-package-icon">
                    <img src="public/res/cocodamol.png" alt="">
                  </div>
                </div>
                <div class="rx-package-name"><span>Codeine</span></div>
            </div>
            <div class="rx-package">
                <div class="rx-package-icon">
                  <div class="rx-package-icon">
                    <img src="public/res/aspirin.png" alt="">
                  </div>
                </div>
                <div class="rx-package-name"><span>Aspirin</span></div>
            </div>
          </div>

          <button data-order="manual_label" class="btn btn-primary btn-order-send">
            Next
          </button>
        </div>
      </div>
      <div class="content-wrapper content-wrapper-postage">
        <div class="info-user">
          <div class="user-instructions">
            <span>print & Place Pilltime label topside of the box</span>
            <img src="public/res/manual_label_steps.jpeg" alt="">
          </div>
        </div>
        <div class="info-rx">
          <div class="info-rx-reprint">
            <button type="button" data-order="royal_label" class="btn btn-primary" disabled name="button">Next</button>
            <button type="button" data-order="reprint" class="btn btn-primary" name="button">Print</button>
          </div>
        </div>
        <div class="info-waiting">
          <span>PLEASE FIND ALL ITEMS...</span>
        </div>
      </div>
    </div>
  </div>
</main>
