<main class="dashboard stock-1 flex-center">
  <div class="operating-system-window">
    <span class="close-btn dot"></span>
    <span class="dot"></span>
    <span class="dot"></span>
  </div>
  <div class="container">
    <?php include 'nav.php' ?>
    <div class="content">
        <div class="content-wrapper">
          <?php include 'filter.php' ?>
          <div class="tooltip tooltip-help aa">
            <span>Select a stock from the list and edit it it's stock levels.</span>
            <img src="public/res/information.svg" alt="">
          </div>
          <div class="header">
            Stock Levels
          </div>
          <ul class="info-header stock">
            <li>ID</li>
            <li>Name</li>
            <li>Stock Quantity</li>
            <li>Manual</li>
            <li>Barcode</li>
            <li>Pack Quantity</li>
            <li>Total Weight (grams)</li>
            <li>Duration Period</li>
            <li class="is-flag">Status</li>
          </ul>
          <div class="info-grid stock-1">
            <table style="width:100%">
            </table>
          </div>
        </div>
        <div class="content-wrapper content-wrapper-editstock">
          <!--<div class="info-user">
            <div class="user-content">
              <div class="user-header">
                <img src="public/res/pills.svg" alt="">
              </div>
              <div class="user-header">
                <span class="user-name"></span>
              </div>
            </div>
            <div class="user-content">
              <div class="form-group">
                <div class="label">

                </div>
                <div class="data">

                </div>
              </div>
              <div class="form-group">
                <div class="label">

                </div>
                <div class="data">

                </div>
              </div>
              <div class="form-group">
                <div class="label">

                </div>
                <div class="data">

                </div>
              </div>
              <div class="form-group">
                <div class="label">

                </div>
                <div class="data">

                </div>
              </div>
              <div class="form-group">
                <div class="label">

                </div>
                <div class="data">

                </div>
              </div>
              <div class="form-group">
                <div class="label">

                </div>
                <div class="data">

                </div>
              </div>
              <div class="form-group">
                <div class="label">

                </div>
                <div class="data">

                </div>
              </div>
              <div class="form-group">
                <div class="label">

                </div>
                <div class="data">

                </div>
              </div>
            </div>
          </div> -->
          <div class="info-pharm">
            <div class="info-waiting">
              <span>PLEASE SELECT A MEDICINE...</span>
            </div>
            <div class="info-pharm-container">
              <div class="form-group data">
                <label for="pharmInput" class="pharm-label"></label>
                <input type="text" class="pharm-input" id="pharmInput">
              </div>
              <div class="form-group data">
                <label for="pharmInput" class="pharm-label"></label>
                <input type="text" class="pharm-input" id="pharmInput">
              </div>
              <div class="form-group data">
                <label for="pharmInput" class="pharm-label"></label>
                <input type="text" class="pharm-input" id="pharmInput">
              </div>
              <div class="form-group data">
                <label for="pharmInput" class="pharm-label"></label>
                <input type="text" class="pharm-input" id="pharmInput">
              </div>
              <div class="form-group form-group--cancel">
                <span class="btn btn-primary">Cancel</span>
              </div>
            </div>
            <div class="info-pharm-container">
              <div class="form-group data">
                <label for="pharmInput" class="pharm-label"></label>
                <input disabled type="text" class="pharm-input" id="pharmInput">
              </div>
              <div class="form-group data">
                <label for="pharmInput" class="pharm-label"></label>
                <input disabled type="text" class="pharm-input" id="pharmInput">
              </div>
              <div class="form-group data">
                <label for="pharmInput" class="pharm-label"></label>
                <input type="text" class="pharm-input" id="pharmInput">
              </div>
              <div class="form-group data">
                <label for="pharmInput" class="pharm-label"></label>
                <input type="text" class="pharm-input" id="pharmInput">
              </div>
              <div class="form-group form-group--save">
                <span class="saved-info"><span>Saved!</span></span>
                <span class="btn btn-primary">Save</span>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
</main>
