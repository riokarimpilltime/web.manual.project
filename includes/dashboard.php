<main class="dashboard flex-center">
  <div class="operating-system-window">
    <span class="close-btn dot"></span>
    <span class="dot"></span>
    <span class="dot"></span>
  </div>
  <div class="container">
    <?php include 'nav.php' ?>
    <div class="content">
        <div class="content-wrapper">
          <?php include 'filter.php' ?>
          <div class="tooltip tooltip-help aa">
            <span>Select a stock from the list and edit it it's stock levels.</span>
            <img src="public/res/information.svg" alt="">
          </div>
          <div class="header">
            Low Stock
          </div>
          <ul class="info-header stock">
            <li>ID</li>
            <li>Name</li>
            <li>Stock Quantity</li>
            <li>Manual</li>
            <li>Barcode</li>
            <li>Pack Quantity</li>
            <li>Total Weight (grams)</li>
            <li>Duration Period</li>
            <li class="is-flag">Status</li>
          </ul>
          <div class="info-grid stock">
            <table style="width:100%">
            </table>
          </div>
        </div>
        <div class="content-wrapper content-wrapper-orders">
          <?php include 'filter.php' ?>
          <div class="tooltip tooltip-help">
            <span>Select an order from the list and proceed to process the order.</span>
            <img src="public/res/information.svg" alt="">
          </div>
          <div class="header">
            Current Orders
          </div>
          <ul class="info-header orders">
            <li>Order ID</li>
            <li>Given Order ID</li>
            <li>Given ID</li>
            <li>Full Name</li>
            <li>Address Line 1</li>
            <li>Address Line 2</li>
            <li>Area</li>
            <li>City</li>
            <li>Country</li>
            <li>Postcode</li>
            <li>Tel Number</li>
            <li>Email</li>
            <li>Preferred Name</li>
          </ul>
          <div class="info-grid orders">
            <table style="width:100%">
            </table>
          </div>
        </div>
      </div>
  </div>
</main>
