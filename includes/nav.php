
  <nav class="dashboard-nav">
    <ul>
      <li>
        <div class="nav-item">
          <img src="public/res/logo.svg"/>
        </div>
      </li>
      <li data-nav="orders" class="active">
        <div class="nav-item">
          <img src="public/res/pills.svg"/><span>Orders</span>
        </div>
      </li>
      <li data-nav="stock">
        <div class="nav-item">
          <img src="public/res/robotic-arm.svg"/><span>Stock</span>
        </div>
      </li>
      <li data-nav="admin">
        <div class="nav-item">
          <img src="public/res/settings.svg"/><span>Admin</span>
        </div>
      </li>
      <li data-nav="reports">
        <div class="nav-item">
          <img src="public/res/growth.svg"/><span>Reports</span>
        </div>
      </li>
      <li class="user-logged">
        <span>
          <strong>Current User:</strong>
          <br>
          Rezza Rio
        </span>
      </li>
      <li class="user-logout">
        <div class="nav-item">
          <img src="public/res/logout.svg"/><span>Logout</span>
        </div>
      </li>
    </ul>
  </nav>
