<div filter class="tooltip tooltip-filter">
  <span>Select a filter to order the stock.</span>
  <img filter src="public/res/filteralt.svg" alt="">
  <ul filter class="filter-nav">
    <li filter class="filter-nav-item">
      <div filter class="filter-header">Sort By</div>
    </li>
    <li filter class="filter-nav-item">
      <select filter class="filter-sort" name="sortby">
        <option filter value="1">ID</option>
        <option filter value="2">Name</option>
        <option filter value="3">Stock Quantity</option>
        <option filter value="4">Pack Quantity</option>
      </select>
    </li>
    <li filter class="filter-nav-item">
      <div filter class="filter-header">Filters</div>
    </li>
    <li filter class="filter-nav-item">
      <label filter for="noStock">All</label>
      <input filter type="checkbox" name="noStock" value="">
    </li>
    <li filter class="filter-nav-item">
      <label filter for="lowStock">Low Stock</label>
      <input filter type="checkbox" name="lowStock" value="">
    </li>
    <li filter class="filter-nav-item">
      <label filter for="actionRequired">Action Required</label>
      <input filter type="checkbox" name="actionRequired" value="">
    </li>
  </ul>
</div>
