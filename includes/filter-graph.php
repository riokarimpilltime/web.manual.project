<div filter class="tooltip tooltip-filter tooltip-graph">
  <span>Select a filter to order the stock.</span>
  <img filter src="public/res/filteralt.svg" alt="">
  <ul filter class="filter-nav">
    <li filter class="filter-nav-item">
      <div filter class="filter-header">Graph Type</div>
    </li>
    <li filter class="filter-nav-item">
      <select filter class="filter-sort" name="graphtype">
        <option filter value="1">Orders</option>
        <option filter value="2">Customers</option>
        <option filter value="3">Stock Levels</option>
      </select>
    </li>
  </ul>
</div>
