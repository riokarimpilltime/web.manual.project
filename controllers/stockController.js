/*jshint esversion: 6 */

function setStock(view, stock){
  _stockItem = [];
  for (let data in stock){
    _stockItem.push(stock[data]);
  }
  view.querySelector('.info-waiting').style.display="none";
  //load in new template
  let x = 0;
  view.querySelectorAll('.info-pharm .form-group.data').forEach(function(formgroup, i){
    formgroup.querySelector('label').innerText = Object.keys(stock)[i].toLowerCase();
    formgroup.querySelector('input').value = stock[Object.keys(stock)[i]];
  });
  //all of the html elements to set order information for
}
  document.querySelector('.form-group--cancel .btn').addEventListener('click', function(){
        document.querySelector('main.stock-1 .info-waiting').setAttribute('style', '');
  });
  document.querySelector('.form-group--save .btn').addEventListener('click', function(){
    document.querySelector('.form-group--save .saved-info').classList.toggle('saved');
  });
