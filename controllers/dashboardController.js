/*jshint esversion: 6 */


init();
function init(){
  let _stockItem = [];
  let _orderItem  = [];
  renderTable();
  handleNav();
  handleFilters();
}

function handleNav(){
  let navSelectors = ['orders', 'stock', 'admin', 'reports', 'home'];
  navSelectors.forEach(function(navItem){
    document.querySelectorAll('li[data-nav="'+ navItem +'"]').forEach(function(navMult){
      navMult.addEventListener('click', function(){
        document.querySelectorAll('li[data-nav]').forEach(function(el){
          if(el.classList.contains('active'))
            el.classList.remove('active');
        });
        document.querySelectorAll('li[data-nav="'+ this.getAttribute('data-nav') +'"]').forEach(function(el){
          el.classList.add('active');
        });
        getPage(this.getAttribute('data-nav'));
      });
    });
  });
}

function renderTable(){
  let orders = Orders.data;

  let stocks = Stock.data;

  for (let i = 0; i<20; i++){
    if(!!document.querySelector('.info-grid.stock table'))
      populateTable(document.querySelector('.info-grid.stock table'), stocks);
    if(!!document.querySelector('.info-grid.stock-1 table'))
      populateTable(document.querySelector('.info-grid.stock-1 table'), stocks, {editable:true});
    if(!!document.querySelector('.info-grid.orders table'))
      populateTable(document.querySelector('.info-grid.orders table'), orders);
  }
}

function populateTable(table, data, options){
  _.each(data, function(obj){
      let entry = document.createElement('tr');
      _.each(obj, function(prop){
        var dataInsert = document.createElement('td');
        dataInsert.innerText = prop;
        entry.appendChild(dataInsert);
      });
      if(_.has(obj, "stockQuantity")){
        let flag = document.createElement('td');
        flag.classList.add('is-flag');
        let flagImg = document.createElement('img');
        if(obj.stockQuantity < 10)
          flagImg.setAttribute('src', "public/res/times-circle.svg");
        else if(!obj.name)
          flagImg.setAttribute('src', "public/res/question-circle.svg");
        else
          flagImg.setAttribute('src', "public/res/check-circle.svg");
        flag.appendChild(flagImg);
        entry.appendChild(flag);
        entry.addEventListener('click', function(){
          if(options && options.editable)
          {
            let view = document.querySelector('.content-wrapper-editstock')
            setStock(view, obj);
          }
          else
          {
            document.querySelectorAll('li[data-nav]').forEach(function(el){
              if(el.classList.contains('active'))
                el.classList.remove('active');
            });
            document.querySelectorAll('li[data-nav="stock"]').forEach(function(listItem){
              listItem.classList.add('active')
            })
            getPage('stock', obj);
          }
        })
      }
      else if (_.has(obj, "givenOrderId")){
        entry.addEventListener('click', function(){
          getPage('orderSet', obj);
        })
      }
      table.appendChild(entry);
  });
}


function getPage(view, options) {
  let page;
  document.querySelectorAll('main').forEach(function(el){
    el.style.display = "none";
  });
  switch(view){
    case "orders":
      page = document.querySelector('main.dashboard');
      break;
    case "orderSet":
      page = document.querySelector('main.orders-1');
      setOrder(page, options);
      break;
    case "stock":
      page = document.querySelector('main.stock-1');
      renderTable();
      break;
    case "admin":
      page = document.querySelector('main.admin-1');
      break;
    case "reports":
      page = document.querySelector('main.reports-1');
      setReports(page);
      break;
  }
  page.style.display = "flex";
}

function handleFilters(){
  document.querySelector('html').addEventListener('click', function(evt){
    if(!evt.srcElement.hasAttribute('filter')){
      document.querySelectorAll('[filter]').forEach(function(filter){
        if(filter.classList.contains('open'))
          filter.classList.remove('open');
      });
    }
  });

  document.querySelectorAll('.tooltip-filter').forEach(function(filter){
    filter.addEventListener('click', function(evt){
      filter.querySelector('.filter-nav').classList.toggle('open');
    });
  });

  document.querySelector('.filter-nav').addEventListener('click', function(evt){
    evt.stopPropagation();
  });
}
