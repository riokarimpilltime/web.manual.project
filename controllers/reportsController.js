function setReports(view, options){
  //todo: delete old instance of graph on reload
  const ordersGraph = view.querySelector('#ordersChart')
  const ordersRender = new Chart(ordersGraph, {
    type:'line',
    data: {
      labels: ["Week 11", "Week 12", "Week 13", "Week 14", "Week 15", "Week 16", "Week 17", "Week 18", "Week 19"],
      datasets: [{
        label: ['Orders'],
        data: [22, 13, 31, 27, 5, 12, 76, 55, 71],
        borderColor: [
          'rgba(54, 162, 235, 1)',
        ],
        backgroundColor:[
          'rgba(0, 0, 0, 0)',
        ]
      }]
    },
    options: {
      title:{
        display:false,
      },
      legend:{
        display:false,
      },
      scales: {
          yAxes: [{
              ticks: {
                  beginAtZero:true
              }
          }]
      },
      responsive: true,
      maintainAspectRatio: true,
    }
  });
}
function setMaps(){
  let latLong = {lat: 54.251186, lng: -4.463196};
  let locations = [
    ['Order 1', latLong],
    ['Order 2', {lat:55.57, lng:-3.463196}],
    ['Order 3', {lat:54.57, lng:-2.463196}],
    ['Order 4', {lat:53.57, lng:-1.463196}],
    ['Order 5', {lat:52.57, lng:-3.163196}],
  ];
  const ordersMap = document.querySelector('#ordersMap');
  const mapRender = new google.maps.Map(ordersMap, {
    center: latLong,
    zoom: 5.8
  })
  let marker, i;
  for (i = 0; i < locations.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i][1], locations[i][2]),
      map: mapRender
    });

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent(locations[i][0]);
        infowindow.open(mapRender, marker);
      }
    })(marker, i));
  }
}
