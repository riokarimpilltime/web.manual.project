/*jshint esversion: 6 */

function setOrder(view, order){
  _orderItem = [];
  orderForm = view.querySelector('.content-wrapper');
  for (let data in order){
    _orderItem.push(order[data]);
  }
  let x = 0;
  view.querySelectorAll('.data').forEach(function(formgroup, i){
    if(Object.keys(order)[i] == "fullName")
        view.querySelector('.user-name').innerText = _orderItem[i];
    view.querySelectorAll('.label')[x].innerText = Object.keys(order)[i];
    view.querySelectorAll('.data')[x].innerText = _orderItem[i];
    x++;
  });
  //all of the html elements to set order information for
}

document.querySelectorAll('[data-order]').forEach(function(orderBtn){
  orderBtn.addEventListener('click', function(evt){
      getStep(evt.srcElement.getAttribute('data-order'), null, evt);
  });
});

document.querySelectorAll('.orders-1 .rx-package').forEach(function(package){
  package.addEventListener('click', function(evt){
    this.classList.toggle('selected');
    let allSelected = true;
    document.querySelectorAll('.orders-1 .rx-package').forEach(function(package){
      if(!package.classList.contains('selected')){
        allSelected=false;
      }
    });
    if(allSelected)
      document.querySelector('.btn-order-send').classList.add('active');
    else{
      if(document.querySelector('.btn-order-send').classList.contains('active'))
        document.querySelector('.btn-order-send').classList.remove('active');
    }
  });
});

function getStep(view, options, event) {
  let rmEl;
  switch(view){
    case "manual_label":
      event.srcElement.style.display = "none";
      rmEl = document.querySelector('.info-waiting');
      rmEl.parentElement.removeChild(rmEl);
      break;
    case "royal_label":
      if(confirm('Please confirm you have placed the Manual label')){
        document.querySelector('.btn[data-order="reprint"]').innerText ="Print";
        event.srcElement.innerText = 'TICK';
        event.srcElement.setAttribute('disabled', '')
        event.srcElement.setAttribute('data-order', 'process');
        let postageDiv = document.querySelector('.content-wrapper-postage');
        let postHeader = postageDiv.querySelector('.user-instructions span');
        let postImg = postageDiv.querySelector('.user-instructions img');
        postHeader.innerText = 'PRINT & PLACE PILLTIME LABEL FRONTSIDE OF THE BOX';
        postImg.setAttribute('src', 'public/res/royal_label_steps.jpeg');
      }
      break;
    case "process":
      if(confirm('Please confirm you have placed the Royal Mail label')){
        rmEl = document.querySelector('.content-wrapper-postage');
        rmEl.parentElement.removeChild(rmEl);
        document.querySelectorAll('.content-wrapper').forEach(function(content){
          content.style.display = "none";
        });
        Promise.resolve(document.querySelector('.orders-1 .content').innerHTML +=
        `
        <div class="content-wrapper content-wrapper-final">
          <div class="content-container">
            <div class="info-final-header">
              Completing Order ${_orderItem[0]}...
            </div>
            <ul class="info-final-list">
              <li>
                Creating Pilltime order
              </li>
              <li>
                Confirming order with Manual
              </li>
              <li>
                Confirming order with Royal Mail
              </li>
              <li>
                Finalising order
              </li>
            </ul>
          </div>
        </div>
        `).then(function(){
          let t = 1000;
          let previousTask;
          document.querySelectorAll('.content-wrapper-final li').forEach(function(orderItem, i){
            setTimeout(function(){
              orderItem.classList.add('ready')
              if(previousTask)
                previousTask.classList.add('complete')
              previousTask = orderItem;
              if(i === (document.querySelectorAll('.content-wrapper-final li').length-1)){
                setTimeout(function(){
                  document.querySelector('.content-wrapper-final').innerHTML = `
                    <span class="order-complete">Order ${_orderItem[0]} Complete</span>
                  `
                  setTimeout(function(){
                    window.location.href='/reset';
                  }, 2000)
                },3000)
              }
            },t)
            t+=2000;

          });
        });

      }
      break;

    case "reprint":
      let btn1 = document.querySelector('.btn[data-order="royal_label"]');
      let btn2 = document.querySelector('.btn[data-order="process"]');
      event.srcElement.innerText = 'Reprint'
      if(confirm('Would you like to reprint Royal Mail / Manual label?')){
        alert("Printing...");
      }
      if(!!btn1)
        btn1.removeAttribute('disabled')
      if(!!btn2)
        btn2.removeAttribute('disabled')
      break;
  }
}
