/*jshint esversion: 6 */

let loginBtn = document.querySelector('[data-handler="loginSubmit"]');
let loginPage = document.querySelector('main.login');
let nextPage = document.querySelector('main.dashboard');
let closeBtn = document.querySelectorAll('.close-btn');
let logoutBtn = document.querySelectorAll('.user-logout');

function getLoginPage(){
  document.querySelectorAll('main').forEach(function(el){
    el.style.display = "none";
  });
  loginPage.style.display = "flex";
};

loginBtn.addEventListener('click', function(){
  loginPage.style.display = "none";
  nextPage.style.display = "flex";

});
closeBtn.forEach(function(btn){
  btn.addEventListener('click', getLoginPage());
});
logoutBtn.forEach(function(btn){
  btn.addEventListener('click', getLoginPage());
});
